<?php 

function db_connect(){
    $connexion = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
    confirm_db_connection($connexion);
    return $connexion;
}

function confirm_db_connection($connexion) {
    if($connexion->connect_errno) {
        $message = "Database connection failed";
        $message .= $connexion->connect_errno;
        $message .= "( " . $connexion->connect_errno . " )";
        exit($message);
    }
}