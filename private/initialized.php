<?php
//routes

// donner accès à tous les dossier du projet 
define('PRIVATE_PATH',dirname(__FILE__));
//donne accès au project path depuis le private path
define('PROJECT_PATH', dirname(PRIVATE_PATH));
//recup le dossier public dispo  grâce à project path depuis le private path
define('PUBLIC_PATH', PROJECT_PATH . '/public');
// recup le dossier shared  dispo dans private path 
define('SHARED_PATH', PRIVATE_PATH . '/shared');


require_once('functions.php');
require_once('db_credentials.php');
require_once('database_functions.php');

$public_end = strpos($_SERVER['SCRIPT_NAME'],'/public');

$doc_root = substr($_SERVER['SCRIPT_NAME'],0,$public_end);

define($_SERVER['HTTP_HOST'],$doc_root);

// connexion à la base de données 

$database = db_connect();

?>