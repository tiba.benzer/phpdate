<?php
function erro_404(){
    header($_SERVER['SERVER_PROTOCOL']. "404 not found");
    exit();
}

function error_500() {
    header($_SERVER['SERVER_PROTOCOL'] . "server error");
    exit();
}

function redirect_to($location) {
    header('Location:' . $location);
    exit();
}

function url_for($script_path){
    if($script_path[0] != '/'){
        $script_path = '/' . $script_path;
    }
    return $script_path;
}

function valid_donnees($donnees) {
    $donnees = trim($donnees);
    $donnees = stripslashes($donnees);
    $donnees = htmlspecialchars($donnees);
    $donnees = htmlentities($donnees);
    $donnees = strip_tags($donnees);
    return $donnees;
}

function trouverSport(string $autre_sport) {
    
    $sql = $database->query("SELECT idsports FROM sports WHERE sport = :sport");
    $insertIntoSports = mysql_query($sql);
     if(!mysql_query($sql, $database)){
         die ("Impossible d'inserer dans la base de données : " . mysql_error());
     }

     mysql_close($database);
} 
